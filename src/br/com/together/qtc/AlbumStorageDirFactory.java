package br.com.together.qtc;

import br.com.together.qtc2.R;
import java.io.File;

abstract class AlbumStorageDirFactory {
	public abstract File getAlbumStorageDir(String albumName);
}
