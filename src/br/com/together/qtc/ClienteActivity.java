package br.com.together.qtc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import br.com.together.qtc.util.Util;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ClienteActivity extends SherlockFragmentActivity implements
		ActionBar.TabListener {

	Cliente cliente;
	int index = -1;
	ActionBar bar;
	boolean leituraInicial = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cliente_main);

		if (savedInstanceState != null) {
			cliente = (Cliente) savedInstanceState.getSerializable("cliente");
			index = savedInstanceState.getInt("index");
			// bar.selectTab(bar.getTabAt(category));
		} else {
			if (getIntent().getExtras() != null) {
				Bundle extras = getIntent().getExtras();
				cliente = (Cliente) extras.getSerializable("cliente");
			}
		}

		

//		if (savedInstanceState != null) {
//			category = savedInstanceState.getInt("category");
//			
//		}
		Util.log("ON CREATE");

	}

	@Override
	protected void onResume() {
		ClienteDetailFragment clienteFrag = (ClienteDetailFragment) getSupportFragmentManager()
				.findFragmentById(R.id.frag_clienteDetails);
		clienteFrag.carregaFragmento(cliente);
		leituraInicial = true;
		// inserido pro absherlock
		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		bar = getSupportActionBar();
		bar.removeAllTabs();
		bar.addTab(bar.newTab().setText("PDV's").setTabListener(this));
		bar.addTab(bar.newTab().setText("Visitas").setTabListener(this));
		bar.addTab(bar.newTab().setText("Resultados").setTabListener(this));

		bar.setDisplayShowHomeEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);
		
		leituraInicial = false;
		if(index > -1)
			bar.setSelectedNavigationItem(index);
		
		super.onResume();
	}
	
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		if(!leituraInicial)
			index = tab.getPosition();
		
		switch (tab.getPosition()) {
		case 0:
			getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.frag_conteudo, ClientePDVsActivity.ClientePDVsFragment.newInstance(cliente)
					).commit();
			
			break;
		case 1:
			getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.frag_conteudo, ClienteVisitasActivity.ClienteVisitasFragment.newInstance(cliente)
					).commit();
			break;

		case 2:
			getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.frag_conteudo, ClientePDVsActivity.ClientePDVsFragment.newInstance(cliente)
					).commit();
			break;

		default:
			break;
		}
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {

	}

	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("cliente", cliente);
		outState.putSerializable("index", bar.getSelectedNavigationIndex());
		super.onSaveInstanceState(outState);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "editar")
				.setIcon(R.drawable.ic_action_editar)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_ALWAYS
								| MenuItem.SHOW_AS_ACTION_WITH_TEXT);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case 0:
			Intent intentEditar = new Intent(this,
					ClienteCadastroActivity.class);
			intentEditar.putExtra("cliente", cliente);
			startActivity(intentEditar);
			return true;
		case android.R.id.home:
			Intent intent = new Intent(this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
