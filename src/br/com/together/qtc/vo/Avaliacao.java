package br.com.together.qtc.vo;

import java.io.Serializable;
import java.util.ArrayList;

public class Avaliacao implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3028233053781752237L;
	private long id = -1;
	private int idVisita;
	private int idCategoria;
	private float rating = 0;
	private String comentarios;
	private ArrayList<String> listaFotosURI = new ArrayList<String>();
	
	public Avaliacao() {
		super();
	}

	public Avaliacao(int idVisita, int idCategoria) {
		super();
		this.idVisita = idVisita;
		this.idCategoria = idCategoria;
	}
	
	public Avaliacao(long id, int idVisita, int idCategoria) {
		super();
		this.id = id;
		this.idVisita = idVisita;
		this.idCategoria = idCategoria;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getIdVisita() {
		return idVisita;
	}

	public void setIdVisita(int idVisita) {
		this.idVisita = idVisita;
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public ArrayList<String> getListaFotosURI() {
		return listaFotosURI;
	}

	public void setListaFotosURI(ArrayList<String> listaFotosURI) {
		this.listaFotosURI = listaFotosURI;
	}
	
	
}
