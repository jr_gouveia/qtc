package br.com.together.qtc.vo;


public class Categoria implements Comparable<Categoria>{
	private int id;
	private String nome;
	private String instrucao;

	public Categoria(int id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Categoria(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getId() {
		return id;
	}

	@Override
	public int compareTo(Categoria another) {
		return (int) (id - another.getId());
	}

	public String getInstrucao() {
		return instrucao;
	}

	public void setInstrucao(String instrucao) {
		this.instrucao = instrucao;
	}

}
