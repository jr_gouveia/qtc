package br.com.together.qtc.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;

public class Visita implements Serializable {

	private static final long serialVersionUID = -1486548352354348491L;
	private int id;
	private PDV pdv;
	private GregorianCalendar data;
	private boolean fechada;

	public Visita() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PDV getPdv() {
		return pdv;
	}

	public void setPdv(PDV pdv) {
		this.pdv = pdv;
	}

	public GregorianCalendar getData() {
		return data;
	}

	public void setData(GregorianCalendar data) {
		this.data = data;
	}

	public boolean isFechada() {
		return fechada;
	}

	public void setFechada(boolean fechada) {
		this.fechada = fechada;
	}

}
