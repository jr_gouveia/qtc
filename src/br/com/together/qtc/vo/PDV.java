package br.com.together.qtc.vo;

import java.io.Serializable;

public class PDV implements Serializable{

	private static final long serialVersionUID = -1486548352354348491L;
	private int id;
	private String nome;
	private String endereco;
	private String telefone;
	private String responsavel;
	
	public PDV() {
	}
	
	public PDV(int id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public PDV(int id, String nome, String endereco, String telefone,
			String responsavel) {
		this.id = id;
		this.nome = nome;
		this.endereco = endereco;
		this.telefone = telefone;
		this.responsavel = responsavel;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	
	
}
