package br.com.together.qtc.vo;

import java.io.Serializable;

public class Cliente implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8151426644555881818L;
	int id;
	String nome;
	String logo;
	String telefone;
	String responsavel;
	
		
	public Cliente(int id, String nome) {
		this.id = id;
		this.nome = nome;
	}
	
	public Cliente(String nome) {
		this.nome = nome;
	}
	
	public Cliente(int id, String nome, String logo) {
		this.id = id;
		this.nome = nome;
		this.logo = logo;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	
}
