package br.com.together.qtc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import br.com.together.qtc2.R;

public class SplashActivity extends Activity  implements Runnable {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        ImageView myImageView= (ImageView)findViewById(R.id.imageLogoSplash);
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.animacao_splash);
        myImageView.startAnimation(myFadeInAnimation);
        Handler h = new Handler();
		h.postDelayed(SplashActivity.this, 3000);
		
    }

	@Override
	public void run() {
		startActivity(new Intent(this, HomeActivity.class));
		finish();
		
	}
}