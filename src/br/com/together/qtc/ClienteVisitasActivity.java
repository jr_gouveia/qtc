package br.com.together.qtc;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import br.com.together.qtc.adapter.VisitaAdapter;
import br.com.together.qtc.util.UtilDB;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc.vo.Visita;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;

public class ClienteVisitasActivity extends SherlockFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.cadastro_cliente);
	}

	public static class ClienteVisitasFragment extends Fragment {
		private Cliente cliente;
		ArrayList<Visita> listProximasVisitas = new ArrayList<Visita>();
		ArrayList<Visita> listUltimasVisitas = new ArrayList<Visita>();

		static ClienteVisitasFragment newInstance(Cliente cliente) {
			ClienteVisitasFragment f = new ClienteVisitasFragment();

			// Supply num input as an argument.
			Bundle args = new Bundle();
			args.putSerializable("cliente", cliente);
			f.setArguments(args);

			return f;
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			cliente = (Cliente) getArguments().getSerializable("cliente");
		}

		/**
		 * The Fragment's UI is just a simple text view showing its instance
		 * number.
		 */
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View v = inflater.inflate(R.layout.visitas_cliente, container, false);
			carregarVisitas(cliente, v);
			return v;
		}

		private void carregarVisitas(Cliente cliente, View view) {
			final Cliente c = cliente;
			ListView listViewProximasVisitas = (ListView) view
					.findViewById(R.id.listViewProximasVisitas);
			ListView listViewVisitasAnteriores = (ListView) view
					.findViewById(R.id.listViewVisitasAnteriores);
			
			//Carrega Próximas visitas
			if (listViewProximasVisitas != null) {
				listProximasVisitas = UtilDB.getVisitas(getActivity()
						.getApplicationContext(), cliente, true);
				if(listProximasVisitas != null && (!listProximasVisitas.isEmpty())){
					listViewProximasVisitas.setAdapter(new VisitaAdapter(getActivity(),
							R.layout.visita_item, listProximasVisitas));
					listViewProximasVisitas.setOnItemClickListener(new OnItemClickListener() {
	
						@Override
						public void onItemClick(AdapterView<?> parent, View v,
								int position, long id) {
							Intent i = new Intent(getActivity()
									.getApplicationContext(),
									AvaliacaoActivity.class);
							i.putExtra("cliente", c);
							i.putExtra("visita", listProximasVisitas.get(position));
							startActivity(i);
						}
					});
					listViewProximasVisitas.setVisibility(View.VISIBLE);
					TextView txtSemProximas = (TextView) view.findViewById(R.id.textViewNenhumaProximaVisita);
					txtSemProximas.setVisibility(View.GONE);
				}else{
					listViewProximasVisitas.setVisibility(View.GONE);
					TextView txtSemProximas = (TextView) view.findViewById(R.id.textViewNenhumaProximaVisita);
					txtSemProximas.setVisibility(View.VISIBLE);
				}
			}

			if (listViewVisitasAnteriores != null) {
				listUltimasVisitas = UtilDB.getVisitas(getActivity()
						.getApplicationContext(), cliente, false);
				if(listUltimasVisitas != null && (!listUltimasVisitas.isEmpty())){
					listViewVisitasAnteriores.setAdapter(new VisitaAdapter(getActivity(),
							R.layout.visita_item, listUltimasVisitas));
					listViewVisitasAnteriores.setOnItemClickListener(new OnItemClickListener() {
	
						@Override
						public void onItemClick(AdapterView<?> parent, View v,
								int position, long id) {
							Intent i = new Intent(getActivity()
									.getApplicationContext(),
									AvaliacaoActivity.class);
							i.putExtra("cliente", c);
							i.putExtra("visita", listUltimasVisitas.get(position));
							i.putExtra("anterior", true);
							startActivity(i);
						}
					});
				}else{
					listViewVisitasAnteriores.setVisibility(View.GONE);
					TextView txtSemAnteriores = (TextView) view.findViewById(R.id.textViewNenhumaVisitaAnterior);
					txtSemAnteriores.setVisibility(View.VISIBLE);
				}
			}
			
			
			Button botao = (Button) view.findViewById(R.id.buttonAdicionarVisita);
			botao.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(
							getActivity().getApplicationContext(),
							VisitaCadastroActivity.class);
					i.putExtra("cliente", c);
					startActivity(i);
				}
			});
		}
	}
}
