package br.com.together.qtc;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import br.com.together.qtc.adapter.PDVAdapter;
import br.com.together.qtc.util.UtilDB;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc.vo.PDV;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;

public class ClientePDVsActivity extends SherlockFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.cadastro_cliente);
	}

	public static class ClientePDVsFragment extends Fragment {
		private Cliente cliente;
		ArrayList<PDV> listPDVs = new ArrayList<PDV>();

		static ClientePDVsFragment newInstance(Cliente cliente) {
			ClientePDVsFragment f = new ClientePDVsFragment();

			// Supply num input as an argument.
			Bundle args = new Bundle();
			args.putSerializable("cliente", cliente);
			f.setArguments(args);

			return f;
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			cliente = (Cliente) getArguments().getSerializable("cliente");
		}

		/**
		 * The Fragment's UI is just a simple text view showing its instance
		 * number.
		 */
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View v = inflater.inflate(R.layout.pdvs_cliente, container, false);
			carregarPDVs(cliente, v);
			return v;
		}

		private void carregarPDVs(Cliente cliente, View view) {
			final Cliente c = cliente;
			ListView listViewPDVs = (ListView) view
					.findViewById(R.id.listViewPDVs);
			if (listViewPDVs != null) {
				listPDVs = UtilDB.getPDVs(getActivity()
						.getApplicationContext(), cliente);
				listViewPDVs.setAdapter(new PDVAdapter(getActivity(),
						R.layout.pdv_item, listPDVs, cliente));
				
			}

			Button botao = (Button) view.findViewById(R.id.buttonAdicionarPDV);
			botao.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(
							getActivity().getApplicationContext(),
							PDVCadastroActivity.class);
					i.putExtra("cliente", c);
					startActivity(i);
				}
			});
		}
	}
}
