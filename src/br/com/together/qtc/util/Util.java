package br.com.together.qtc.util;

import java.util.ArrayList;

import br.com.together.qtc.vo.Categoria;

import android.util.Log;

public class Util {

	private static String TAG = "QTC";
	
	public static void log(String mensagem){
		Log.d(TAG, mensagem);
	}
	public static void log(String mensagem, Throwable erro){
		if(erro != null){
			Log.e(TAG, mensagem, erro);
		}else{
			Log.e(TAG, mensagem);
		}
	}
	
	public static ArrayList<Categoria> inicializaCategorias(){
		ArrayList<Categoria> listaCategorias = new ArrayList<Categoria>();
		listaCategorias.add(new Categoria(1, "Estocagem"));
		listaCategorias.add(new Categoria(2, "Reposição"));
		listaCategorias.add(new Categoria(3, "Visibilidade"));
		listaCategorias.add(new Categoria(4, "Acesso ao cliente"));
	
		return listaCategorias;
	}
	
}
