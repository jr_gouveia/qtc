package br.com.together.qtc.util;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.together.qtc.db.QTCOpenHelper;
import br.com.together.qtc.vo.Avaliacao;
import br.com.together.qtc.vo.Categoria;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc.vo.PDV;
import br.com.together.qtc.vo.Visita;

public class UtilDB {

	// CLIENTE
	public static ArrayList<Cliente> getClientes(Context context) {
		Cliente adicionar = new Cliente(-1, "Adicionar um novo Cliente");
		SQLiteDatabase db = null;
		QTCOpenHelper goh = new QTCOpenHelper(context);
		db = goh.getReadableDatabase();
		ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();
		try {

			String query = "select * from " + QTCOpenHelper.CLIENTE_TABLE_NAME
					+ " order by nome";
			Util.log(query);
			Cursor cursor = db.rawQuery(query, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {
					int id = cursor
							.getInt(cursor.getColumnIndex("_id_cliente"));
					String nome = cursor.getString(cursor
							.getColumnIndex("nome"));
					String nomeResponsavel = cursor.getString(cursor
							.getColumnIndex("nome_responsavel"));
					String telefone = cursor.getString(cursor
							.getColumnIndex("telefone"));
					String logo = cursor.getString(cursor
							.getColumnIndex("logo"));

					Cliente cliente = new Cliente(id, nome, logo);
					cliente.setResponsavel(nomeResponsavel);
					cliente.setTelefone(telefone);
					listaClientes.add(cliente);
					Util.log("id: " + id + "    nome: " + nome + "    logo: "
							+ logo);
					cursor.moveToNext();
				} while (!cursor.isAfterLast());

				// fecha o cursor e o banco e retorna o objeto encontrado
				cursor.close();
				fechaDatabase(db);
				listaClientes.add(adicionar);
				return listaClientes;
			}
			cursor.close();
			fechaDatabase(db);
		} catch (Exception e) {
			Util.log("Erro na busca de clientes", e);
			fechaDatabase(db);
		}
		listaClientes.add(adicionar);
		return listaClientes;
	}

	public static long saveCliente(Context context, Cliente novoCliente) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("nome", novoCliente.getNome());
			values.put("logo", novoCliente.getLogo());
			values.put("nome_responsavel", novoCliente.getResponsavel());
			values.put("telefone", novoCliente.getTelefone());

			long id = db.insert(QTCOpenHelper.CLIENTE_TABLE_NAME, null, values);
			if (id != -1) {
				fechaDatabase(db);
				return id;
			}
		} catch (Exception e) {
			Util.log("Erro na inserção de cliente", e);
		}
		fechaDatabase(db);
		return -1;
	}

	public static long editCliente(Context context, Cliente cliente) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("nome", cliente.getNome());
			values.put("logo", cliente.getLogo());
			values.put("nome_responsavel", cliente.getResponsavel());
			values.put("telefone", cliente.getTelefone());

			long qtd = db.update(QTCOpenHelper.CLIENTE_TABLE_NAME, values,
					"_id_cliente = " + cliente.getId(), null);
			if (qtd != -1) {
				fechaDatabase(db);
				return cliente.getId();
			}
		} catch (Exception e) {
			Util.log("Erro na edição de cliente", e);
		}
		fechaDatabase(db);
		return -1;
	}

	public static boolean deleteCliente(Context context, int idCliente) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			// ContentValues values = new ContentValues();

			long id = db.delete(QTCOpenHelper.CLIENTE_TABLE_NAME,
					"_id_cliente=" + idCliente, null);
			if (id > 0) {
				fechaDatabase(db);
				return true;
			}
		} catch (Exception e) {
			Util.log("Erro na deleção de cliente", e);
		}
		fechaDatabase(db);
		return false;
	}

	// FIM CLIENTE

	// CATEGORIA
	public static Categoria getCategoria(Context context, int idCat) {
		SQLiteDatabase db = null;
		QTCOpenHelper goh = new QTCOpenHelper(context);
		db = goh.getReadableDatabase();
		Categoria categoria = null;
		try {

			String query = "select * from "
					+ QTCOpenHelper.CATEGORIAS_TABLE_NAME
					+ " where _id_categoria = " + idCat + " order by categoria";
			Util.log(query);
			Cursor cursor = db.rawQuery(query, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {
					int id = cursor.getInt(cursor
							.getColumnIndex("_id_categoria"));
					String nome = cursor.getString(cursor
							.getColumnIndex("categoria"));

					categoria = new Categoria(id, nome);
					Util.log("id: " + id + "    nome: " + nome);
					cursor.moveToNext();
				} while (!cursor.isAfterLast());

				// fecha o cursor e o banco e retorna o objeto encontrado
				cursor.close();
				fechaDatabase(db);
				return categoria;
			}
			cursor.close();
			fechaDatabase(db);
		} catch (Exception e) {
			Util.log("Erro na busca de categorias", e);
			fechaDatabase(db);
		}

		return categoria;
	}

	public static ArrayList<Categoria> getCategorias(Context context) {
		SQLiteDatabase db = null;
		QTCOpenHelper goh = new QTCOpenHelper(context);
		db = goh.getReadableDatabase();
		ArrayList<Categoria> listaCategoria = new ArrayList<Categoria>();
		try {

			String query = "select * from "
					+ QTCOpenHelper.CATEGORIAS_TABLE_NAME
					+ " order by categoria";
			Util.log(query);
			Cursor cursor = db.rawQuery(query, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {
					int id = cursor.getInt(cursor
							.getColumnIndex("_id_categoria"));
					String nome = cursor.getString(cursor
							.getColumnIndex("categoria"));
					String instrucao = cursor.getString(cursor
							.getColumnIndex("texto"));
					
					Categoria categoria = new Categoria(id, nome);
					categoria.setInstrucao(instrucao);
					listaCategoria.add(categoria);
					Util.log("id: " + id + "    nome: " + nome);
					cursor.moveToNext();
				} while (!cursor.isAfterLast());

				// fecha o cursor e o banco e retorna o objeto encontrado
				cursor.close();
				fechaDatabase(db);
				return listaCategoria;
			}
			cursor.close();
			fechaDatabase(db);
		} catch (Exception e) {
			Util.log("Erro na busca de categorias", e);
			fechaDatabase(db);
		}

		return listaCategoria;
	}

	public static boolean saveCategoria(Context context, Categoria novaCategoria) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("categoria", novaCategoria.getNome());
			values.put("texto", novaCategoria.getInstrucao());

			long id = db.insert(QTCOpenHelper.CATEGORIAS_TABLE_NAME, null,
					values);
			if (id != -1) {
				fechaDatabase(db);
				return true;
			}
		} catch (Exception e) {
			Util.log("Erro na inserção de categoria", e);
		}
		fechaDatabase(db);
		return false;
	}
	
	public static boolean editCategoria(Context context, Categoria categoria) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("categoria", categoria.getNome());
			values.put("texto", categoria.getInstrucao());
			
			
			long qtd = db.update(QTCOpenHelper.CATEGORIAS_TABLE_NAME, values,
					"_id_categoria = " + categoria.getId(), null);

			if (qtd != -1) {
				fechaDatabase(db);
				return true;
			}
		} catch (Exception e) {
			Util.log("Erro na inserção de categoria", e);
		}
		fechaDatabase(db);
		return false;
	}

	public static boolean deleteCategoria(Context context, int idCategoria) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			// ContentValues values = new ContentValues();

			long id = db.delete(QTCOpenHelper.CATEGORIAS_TABLE_NAME,
					"_id_categoria=" + idCategoria, null);
			if (id > 0) {
				fechaDatabase(db);
				return true;
			}
		} catch (Exception e) {
			Util.log("Erro na deleção de categoria", e);
		}
		fechaDatabase(db);
		return false;
	}

	// FIM CATEGORIA

	// CATEGORIAS do CLIENTE
	public static ArrayList<Categoria> getCategoriasDoCliente(Context context,
			Cliente cliente) {
		SQLiteDatabase db = null;
		QTCOpenHelper goh = new QTCOpenHelper(context);
		db = goh.getReadableDatabase();
		ArrayList<Categoria> listaCategoria = new ArrayList<Categoria>();
		try {

			String query = "select c.* from "
					+ QTCOpenHelper.CLIENTES_CATEGORIAS_TABLE_NAME
					+ " as cc join "
					+ QTCOpenHelper.CATEGORIAS_TABLE_NAME
					+ "  as c on cc.id_categoria = c._id_categoria where cc.id_cliente = "
					+ cliente.getId() + " order by c.categoria";
			Util.log(query);
			Cursor cursor = db.rawQuery(query, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {
					int id = cursor.getInt(cursor
							.getColumnIndex("_id_categoria"));
					String nome = cursor.getString(cursor
							.getColumnIndex("categoria"));

					Categoria categoria = new Categoria(id, nome);
					listaCategoria.add(categoria);
					Util.log("id: " + id + "    nome: " + nome);
					cursor.moveToNext();
				} while (!cursor.isAfterLast());

				// fecha o cursor e o banco e retorna o objeto encontrado
				cursor.close();
				fechaDatabase(db);
				return listaCategoria;
			}
			cursor.close();
			fechaDatabase(db);
		} catch (Exception e) {
			Util.log("Erro na busca de categorias", e);
			fechaDatabase(db);
		}

		return listaCategoria;
	}

	public static boolean saveCategoriaDoCliente(Context context,
			long[] idsCategorias, long idCliente) {
		SQLiteDatabase db = null;
		boolean gravou = false;
		try {
			deleteCategoriaDoCliente(context, idCliente);
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			for (int i = 0; i < idsCategorias.length; i++) {
				long idCategoria = idsCategorias[i];
				ContentValues values = new ContentValues();
				values.put("id_categoria", idCategoria);
				values.put("id_cliente", idCliente);
				Util.log("id_categoria  " + idCategoria
						+ "             id_cliente  " + idCliente);
				long id = db.insert(
						QTCOpenHelper.CLIENTES_CATEGORIAS_TABLE_NAME, null,
						values);
				if (id != -1) {
					gravou = true;
				}
			}
			if (gravou) {
				fechaDatabase(db);
				return true;
			}
		} catch (Exception e) {
			Util.log("Erro na inserção de categoriade um cliente", e);
		}
		fechaDatabase(db);
		return false;
	}

	public static boolean deleteCategoriaDoCliente(Context context,
			long idCliente) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			// ContentValues values = new ContentValues();

			long id = db.delete(QTCOpenHelper.CLIENTES_CATEGORIAS_TABLE_NAME,
					"id_cliente=" + idCliente, null);
			if (id > 0) {
				fechaDatabase(db);
				return true;
			}
		} catch (Exception e) {
			Util.log("Erro na deleção de categorias de um cliente", e);
		}
		fechaDatabase(db);
		return false;
	}

	// FIM CATEGORIA

	// PDV
	public static ArrayList<PDV> getPDVs(Context context, Cliente cliente) {
		SQLiteDatabase db = null;
		ArrayList<PDV> listaPDVs = new ArrayList<PDV>();
		if (cliente != null) {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getReadableDatabase();
			try {

				String query = "select * from " + QTCOpenHelper.PDVS_TABLE_NAME
						+ " where id_cliente = " + cliente.getId()
						+ " order by nome";
				Util.log(query);
				Cursor cursor = db.rawQuery(query, null);
				if (cursor.getCount() > 0) {
					cursor.moveToFirst();
					do {
						int id = cursor
								.getInt(cursor.getColumnIndex("_id_pdv"));
						String nome = cursor.getString(cursor
								.getColumnIndex("nome"));
						String nomeResponsavel = cursor.getString(cursor
								.getColumnIndex("nome_responsavel"));
						String telefone = cursor.getString(cursor
								.getColumnIndex("telefone"));
						String endereco = cursor.getString(cursor
								.getColumnIndex("endereco"));

						PDV pdv = new PDV(id, nome, endereco, telefone,
								nomeResponsavel);
						listaPDVs.add(pdv);
						Util.log("id: " + id + "    nome: " + nome);
						cursor.moveToNext();
					} while (!cursor.isAfterLast());

					// fecha o cursor e o banco e retorna o objeto encontrado
					cursor.close();
					fechaDatabase(db);
					return listaPDVs;
				}
				cursor.close();
				fechaDatabase(db);
			} catch (Exception e) {
				Util.log("Erro na busca de clientes", e);
				fechaDatabase(db);
			}
		}
		return listaPDVs;
	}

	public static long savePDV(Context context, PDV novoPDV, Cliente cliente) {
		if (cliente != null) {
			SQLiteDatabase db = null;
			try {
				QTCOpenHelper goh = new QTCOpenHelper(context);
				db = goh.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put("nome", novoPDV.getNome());
				values.put("endereco", novoPDV.getEndereco());
				values.put("nome_responsavel", novoPDV.getResponsavel());
				values.put("telefone", novoPDV.getTelefone());
				values.put("id_cliente", cliente.getId());

				long id = db
						.insert(QTCOpenHelper.PDVS_TABLE_NAME, null, values);
				if (id != -1) {
					fechaDatabase(db);
					return id;
				}
			} catch (Exception e) {
				Util.log("Erro na inserção de pdv", e);
			}
			fechaDatabase(db);
		}
		return -1;
	}

	public static long editPDV(Context context, PDV pdv) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("nome", pdv.getNome());
			values.put("endereco", pdv.getEndereco());
			values.put("nome_responsavel", pdv.getResponsavel());
			values.put("telefone", pdv.getTelefone());

			long qtd = db.update(QTCOpenHelper.PDVS_TABLE_NAME, values,
					"_id_pdv = " + pdv.getId(), null);
			if (qtd != -1) {
				fechaDatabase(db);
				return pdv.getId();
			}
		} catch (Exception e) {
			Util.log("Erro na edição de pdv", e);
		}
		fechaDatabase(db);
		return -1;
	}

	public static boolean deletePDV(Context context, int idPDV) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			// ContentValues values = new ContentValues();

			long id = db.delete(QTCOpenHelper.PDVS_TABLE_NAME, "_id_pdv="
					+ idPDV, null);
			if (id > 0) {
				fechaDatabase(db);
				return true;
			}
		} catch (Exception e) {
			Util.log("Erro na deleção de pdv	", e);
		}
		fechaDatabase(db);
		return false;
	}

	// FIM PDVS

	// VISITAS
	public static ArrayList<Visita> getVisitas(Context context,
			Cliente cliente, boolean proximas) {
		SQLiteDatabase db = null;
		ArrayList<Visita> listaVisitas = new ArrayList<Visita>();
		if (cliente != null) {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getReadableDatabase();
			try {
				long dataAtual = new GregorianCalendar().getTimeInMillis(); // Date().getTime();
				String query = "select v._id_visita, v.data, p.* from "
						+ QTCOpenHelper.PDVS_VISITAS_TABLE_NAME
						+ " as v  join " + QTCOpenHelper.PDVS_TABLE_NAME
						+ " as p on v.id_pdv = p._id_pdv "
						+ " where v.id_cliente = " + cliente.getId();
				if (proximas) {
					query = query + " and data >= '" + dataAtual + "' "
							+ " order by data asc";
				} else {
					query = query + " and data < '" + dataAtual + "' "
							+ " order by data desc";
				}

				Util.log(query);
				Cursor cursor = db.rawQuery(query, null);
				if (cursor.getCount() > 0) {
					cursor.moveToFirst();
					do {
						int id = cursor.getInt(cursor
								.getColumnIndex("_id_visita"));
						int id_pdv = cursor.getInt(cursor
								.getColumnIndex("_id_pdv"));
						String nome = cursor.getString(cursor
								.getColumnIndex("nome"));
						String nomeResponsavel = cursor.getString(cursor
								.getColumnIndex("nome_responsavel"));
						String telefone = cursor.getString(cursor
								.getColumnIndex("telefone"));
						String endereco = cursor.getString(cursor
								.getColumnIndex("endereco"));
						String strData = cursor.getString(cursor
								.getColumnIndex("data"));
						GregorianCalendar data = null;
						try {
							data = new GregorianCalendar();
							data.setTimeInMillis(Long.parseLong(strData));
						} catch (Exception e) {
							Util.log("erro", e);
							data = null;
						}

						PDV pdv = new PDV(id_pdv, nome, endereco, telefone,
								nomeResponsavel);
						Visita visita = new Visita();
						visita.setId(id);
						visita.setPdv(pdv);
						visita.setData(data);
						listaVisitas.add(visita);
						Util.log("id: " + id + "    nome: " + nome);
						cursor.moveToNext();
					} while (!cursor.isAfterLast());

					// fecha o cursor e o banco e retorna o objeto encontrado
					cursor.close();
					fechaDatabase(db);
					return listaVisitas;
				}
				cursor.close();
				fechaDatabase(db);
			} catch (Exception e) {
				Util.log("Erro na busca de visitas", e);
				fechaDatabase(db);
			}
		}
		return listaVisitas;
	}

	public static long saveVisita(Context context, Visita novaVisita,
			Cliente cliente) {
		if (cliente != null) {
			SQLiteDatabase db = null;
			try {
				QTCOpenHelper goh = new QTCOpenHelper(context);
				db = goh.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put("id_pdv", novaVisita.getPdv().getId());
				values.put("id_cliente", cliente.getId());
				values.put("data", novaVisita.getData().getTimeInMillis());

				long id = db.insert(QTCOpenHelper.PDVS_VISITAS_TABLE_NAME,
						null, values);
				if (id != -1) {
					fechaDatabase(db);
					return id;
				}
			} catch (Exception e) {
				Util.log("Erro na inserção de visita", e);
			}
			fechaDatabase(db);
		}
		return -1;
	}

	public static long editVisita(Context context, Visita visita) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("id_pdv", visita.getPdv().getId());
			values.put("id_cliente", visita.getId());
			values.put("data", visita.getData().getTimeInMillis());

			long qtd = db.update(QTCOpenHelper.PDVS_VISITAS_TABLE_NAME, values,
					"_id_visita = " + visita.getId(), null);
			if (qtd != -1) {
				fechaDatabase(db);
				return visita.getId();
			}
		} catch (Exception e) {
			Util.log("Erro na edição de visita", e);
		}
		fechaDatabase(db);
		return -1;
	}

	public static boolean deleteVisita(Context context, int idVisita) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();

			long id = db.delete(QTCOpenHelper.PDVS_VISITAS_TABLE_NAME,
					"_id_visita=" + idVisita, null);
			if (id > 0) {
				fechaDatabase(db);
				return true;
			}
		} catch (Exception e) {
			Util.log("Erro na deleção de visita	", e);
		}
		fechaDatabase(db);
		return false;
	}

	// FIM VISITA

	// AVALIACAO
	public static Avaliacao getAvaliacao(Context context, int idVisita,
			int idCategoria) {
		SQLiteDatabase db = null;

		if (idVisita > 0 && idCategoria > 0) {
			Avaliacao avaliacao = new Avaliacao(idVisita, idCategoria);
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getReadableDatabase();
			try {
				String query = "select * from "
						+ QTCOpenHelper.AVALIACAO_TABLE_NAME
						+ " where id_visita = " + avaliacao.getIdVisita()
						+ " and id_categoria = " + avaliacao.getIdCategoria()
						+ " limit 1";
				Util.log(query);
				Cursor cursor = db.rawQuery(query, null);
				if (cursor.getCount() > 0) {
					cursor.moveToFirst();
					do {
						int id = cursor.getInt(cursor
								.getColumnIndex("_id_avaliacao"));
						String comentarios = cursor.getString(cursor
								.getColumnIndex("comentarios"));
						float rating = cursor.getFloat(cursor
								.getColumnIndex("nota"));

						avaliacao.setId(id);
						avaliacao.setComentarios(comentarios);
						avaliacao.setRating(rating);
						// LOCAL PARA RECUPERAR AS FOTOS
						Util.log("id: " + id + "    nota: " + rating);
						cursor.moveToNext();
					} while (!cursor.isAfterLast());

					// fecha o cursor e o banco e retorna o objeto encontrado
					cursor.close();
					fechaDatabase(db);
					return avaliacao;
				}
				cursor.close();
				fechaDatabase(db);
			} catch (Exception e) {
				Util.log("Erro na busca de visitas", e);
				fechaDatabase(db);
			}
		}
		return null;
	}

	public static long saveAvaliacao(Context context, Avaliacao avaliacao) {
		if (avaliacao != null) {
			SQLiteDatabase db = null;
			try {
				QTCOpenHelper goh = new QTCOpenHelper(context);
				db = goh.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put("id_visita", avaliacao.getIdVisita());
				values.put("id_categoria", avaliacao.getIdCategoria());
				values.put("nota", avaliacao.getRating());
				values.put("comentarios", avaliacao.getComentarios());

				long id = db.insert(QTCOpenHelper.AVALIACAO_TABLE_NAME, null,
						values);
				if (id != -1) {
					avaliacao.setId(id);
					fechaDatabase(db);
					saveFotosAvaliacao(context, avaliacao.getId(), avaliacao.getListaFotosURI());
					calculaMedia(context, avaliacao.getIdVisita());
					return id;
				}
			} catch (Exception e) {
				Util.log("Erro na inserção de visita", e);
			}
			fechaDatabase(db);
		}
		return -1;
	}

	public static long editAvaliacao(Context context, Avaliacao avaliacao) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("id_visita", avaliacao.getIdVisita());
			values.put("id_categoria", avaliacao.getIdCategoria());
			values.put("nota", avaliacao.getRating());
			values.put("comentarios", avaliacao.getComentarios());

			long qtd = db.update(QTCOpenHelper.AVALIACAO_TABLE_NAME, values,
					"_id_avaliacao = " + avaliacao.getId(), null);
			fechaDatabase(db);
			if (qtd != -1) {
				saveFotosAvaliacao(context, avaliacao.getId(), avaliacao.getListaFotosURI());
				calculaMedia(context, avaliacao.getIdVisita());
				return avaliacao.getId();
			}
		} catch (Exception e) {
			Util.log("Erro na edição de visita", e);
		}
		fechaDatabase(db);
		return -1;
	}
	public static void calculaMedia(Context context, long idVisita){
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();
			
			String query = "select avg(nota) as media from "
					+ QTCOpenHelper.AVALIACAO_TABLE_NAME
					+ " where id_visita = " + idVisita;
			Util.log(query);
			Cursor cursor = db.rawQuery(query, null);
			Util.log("qtd resultados: "+cursor.getCount());
			
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {
					float media = cursor.getFloat(cursor
							.getColumnIndex("media"));
					Util.log("media calculada: "+media);
					cursor.moveToNext();
				} while (!cursor.isAfterLast());
			}
			cursor.close();
			fechaDatabase(db);
			
//			ContentValues values = new ContentValues();
//			values.put("id_visita", avaliacao.getIdVisita());
//			values.put("id_categoria", avaliacao.getIdCategoria());
//			values.put("nota", avaliacao.getRating());
//			values.put("comentarios", avaliacao.getComentarios());
//
//			long qtd = db.update(QTCOpenHelper.AVALIACAO_TABLE_NAME, values,
//					"_id_avaliacao = " + avaliacao.getId(), null);
//			if (qtd != -1) {
//				saveFotosAvaliacao(context, avaliacao.getId(), avaliacao.getListaFotosURI());
//
//				fechaDatabase(db);
//				return avaliacao.getId();
//			}
		} catch (Exception e) {
			Util.log("Erro na edição de visita", e);
		}
		fechaDatabase(db);
	}
	// public static boolean deleteVisita(Context context, int idVisita) {
	// SQLiteDatabase db = null;
	// try {
	// QTCOpenHelper goh = new QTCOpenHelper(context);
	// db = goh.getWritableDatabase();
	//
	// long id = db.delete(QTCOpenHelper.PDVS_VISITAS_TABLE_NAME, "_id_visita="
	// + idVisita, null);
	// if (id > 0) {
	// fechaDatabase(db);
	// return true;
	// }
	// } catch (Exception e) {
	// Util.log("Erro na deleção de visita	", e);
	// }
	// fechaDatabase(db);
	// return false;
	// }

	// FIM AVALIACAO

	// FOTOS
	public static ArrayList<String> getFotosAvaliacao(Context context,
			long idAvaliacao) {
		SQLiteDatabase db = null;
		ArrayList<String> listaFotos = new ArrayList<String>();
		if (idAvaliacao > 0) {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getReadableDatabase();
			try {
				String query = "select * from "
						+ QTCOpenHelper.FOTOS_TABLE_NAME
						+ " where id_avaliacao = " + idAvaliacao;
				Util.log(query);
				Cursor cursor = db.rawQuery(query, null);
				Util.log("fotos encontradas: "+cursor.getCount());
				
				if (cursor.getCount() > 0) {
					cursor.moveToFirst();
					do {
						String uri = cursor.getString(cursor
								.getColumnIndex("uri"));

						listaFotos.add(uri);
						cursor.moveToNext();
					} while (!cursor.isAfterLast());

					// fecha o cursor e o banco e retorna o objeto encontrado
					cursor.close();
					fechaDatabase(db);
					return listaFotos;
				}
				cursor.close();
				fechaDatabase(db);
			} catch (Exception e) {
				Util.log("Erro na busca de visitas", e);
				fechaDatabase(db);
			}
		}
		return listaFotos;
	}

	public static void saveFotosAvaliacao(Context context, long idAvaliacao,
			ArrayList<String> listaFotos) {
		if (idAvaliacao > 0) {
			SQLiteDatabase db = null;
			try {
				deleteFotosAvaliacao(context, idAvaliacao);
				QTCOpenHelper goh = new QTCOpenHelper(context);
				db = goh.getWritableDatabase();
				for (String uri : listaFotos) {
					Util.log("salvando foto "+uri);
					ContentValues values = new ContentValues();
					values.put("id_avaliacao", idAvaliacao);
					values.put("uri", uri);
					db.insert(QTCOpenHelper.FOTOS_TABLE_NAME,
							null, values);
				}
				fechaDatabase(db);
			} catch (Exception e) {
				Util.log("Erro na inserção de visita", e);
			}
			fechaDatabase(db);
		}
	}

	public static boolean deleteFotosAvaliacao(Context context, long idAvaliacao) {
		SQLiteDatabase db = null;
		try {
			QTCOpenHelper goh = new QTCOpenHelper(context);
			db = goh.getWritableDatabase();

			long qtd = db.delete(QTCOpenHelper.FOTOS_TABLE_NAME,
					"id_Avaliacao=" + idAvaliacao, null);
			if (qtd > 0) {
				fechaDatabase(db);
				return true;
			}
		} catch (Exception e) {
			Util.log("Erro na deleção de fotos	", e);
		}
		fechaDatabase(db);
		return false;
	}

	// FIM FOTOS

	// Fecha o database
	private static void fechaDatabase(SQLiteDatabase db) {
		if (db != null && db.isOpen())
			db.close();
	}

}
