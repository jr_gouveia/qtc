package br.com.together.qtc;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import br.com.together.qtc.util.UtilDB;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc.vo.PDV;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class PDVCadastroActivity extends SherlockFragmentActivity {
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	String imagemUri;
	private Cliente cliente;
	private PDV pdv;
	EditText txtNome;
	EditText txtTelefone;
	EditText txtResponsavel;
	EditText txtEndereco;
	public static int MODO_EDICAO = 1;
	public static int MODO_INSERCAO = 2;
	private int modo = 2;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.cadastro_pdv);

		txtNome = (EditText) findViewById(R.id.EditTextNome);
		txtTelefone = (EditText) findViewById(R.id.editTextTelefone);
		txtResponsavel = (EditText) findViewById(R.id.EditTextResposavel);
		txtEndereco = (EditText) findViewById(R.id.editTextEndereco);
		if (getIntent().getExtras() != null) {
			Bundle extras = getIntent().getExtras();
			if(extras.containsKey("cliente")){
				cliente = (Cliente) extras.getSerializable("cliente");
				populaDadosCliente(cliente);
			}else{
				cliente = null;
			}
			
			if(extras.containsKey("pdv")){
				pdv = (PDV) extras.getSerializable("pdv");
				modo = MODO_EDICAO;
				populaDados(pdv);
			}else{
				pdv = null;
			}
		}



		ActionBar bar = getSupportActionBar();

		bar.setTitle("Cadastro de PDV");
		bar.setDisplayShowHomeEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);


		Button botaoSalvar = (Button) findViewById(R.id.buttonSalvarPDV);
		botaoSalvar.setOnClickListener(listenerSalvar);
		Button botaoCancelar = (Button) findViewById(R.id.buttonCancelarPDV);
		botaoCancelar.setOnClickListener(listenerCancelar);
	}


	OnClickListener listenerSalvar = new Button.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			long idCliente =  salvarPDV();
			finish();
		}
	};

	OnClickListener listenerCancelar = new Button.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			finish();
		}
	};

	private void populaDados(PDV pdv){
		txtNome.setText(pdv.getNome());
		txtTelefone.setText(pdv.getTelefone());
		txtResponsavel.setText(pdv.getResponsavel());
		txtEndereco.setText(pdv.getEndereco());
	}

	private void populaDadosCliente(Cliente c){
		ClienteDetailFragment clienteFrag = (ClienteDetailFragment) getSupportFragmentManager()
				.findFragmentById(R.id.frag_clienteDetails);
		clienteFrag.carregaFragmento(cliente);

	}

	private long salvarPDV() {
		//		txtNome = (EditText) findViewById(R.id.EditTextNome);
		//		txtTelefone = (EditText) findViewById(R.id.editTextTelefone);
		//		txtResponsavel = (EditText) findViewById(R.id.EditTextResposavel);
		String nome = "";
		String telefone = "";
		String responsavel = "";
		String endereco = "";
		if (txtNome.getText().toString().trim().length() > 0) {
			nome = txtNome.getText().toString();
			telefone = txtTelefone.getText().toString();
			responsavel = txtResponsavel.getText().toString();
			endereco = txtEndereco.getText().toString();
			if(modo == MODO_EDICAO){
				pdv.setNome(nome);
				pdv.setTelefone(telefone);
				pdv.setResponsavel(responsavel);
				pdv.setEndereco(endereco);
				return UtilDB.editPDV(getApplicationContext(), pdv);
			}else{
				PDV novoPDV = new PDV();
				novoPDV.setNome(nome);
				novoPDV.setTelefone(telefone);
				novoPDV.setResponsavel(responsavel);
				novoPDV.setEndereco(endereco);
				return UtilDB.savePDV(getApplicationContext(), novoPDV, cliente);
			}
		}
		return -1;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case android.R.id.home:
			//			Intent intent = new Intent(this, HomeActivity.class);
			//			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//			startActivity(intent);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
