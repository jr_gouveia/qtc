package br.com.together.qtc;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.GridView;
import br.com.together.qtc.adapter.ClienteAdapter;
import br.com.together.qtc.util.UtilDB;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class HomeActivity extends SherlockFragmentActivity {
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */

	private ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();
	View layout;
	String imagemUri;
	AlertDialog.Builder builder;
	EditText txtNome;
	int posicaoSelecionada;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		carregaGridClientes();

	}
	
	@Override
	protected void onResume() {
		carregaGridClientes();
		super.onResume();
	}

	private void carregaGridClientes(){
		listaClientes = UtilDB.getClientes(getApplicationContext());
		GridView gridView = (GridView) findViewById(R.id.gridClientes);
		gridView.setVisibility(View.VISIBLE);
		gridView.setAdapter(new ClienteAdapter(HomeActivity.this,
				R.layout.cliente_item, listaClientes));

		gridView.setOnItemClickListener(listener);
		gridView.setOnItemLongClickListener(listenerLong);
	}

	OnItemClickListener listener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position,
				long id) {
			Cliente clienteSelecionado = listaClientes.get(position);
			if (clienteSelecionado.getId() == -1) {
//				imagemUri = "";
//				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//				layout = inflater.inflate(R.layout.dialog_cadastro_cliente,
//						(ViewGroup) findViewById(R.id.rootCadastro));
//				builder = new AlertDialog.Builder(HomeActivity.this)
//				.setView(layout);
//				builder.setMessage("Cadastro de um novo cliente")
//				.setCancelable(false)
//				.setPositiveButton("Salvar",
//						new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,
//							int id) {
//						boolean inseriu = salvarCliente();
//						if (inseriu) {
//							carregaGridClientes();
//						}
//					}
//				})
//				.setNegativeButton("Cancelar",
//						new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,
//							int id) {
//						dialog.cancel();
//					}
//				});
//
//				builder.show();
//
//				Button buttonLoadImage = (Button) layout
//						.findViewById(R.id.buttonCarregaLogo);
//				buttonLoadImage.setOnClickListener(listenerBotao);
				
				Intent i = new Intent(HomeActivity.this, ClienteCadastroActivity.class);
				startActivity(i);
			}else{
				Intent i = new Intent("br.com.together.qtc.intentcliente");
				i.putExtra("cliente", clienteSelecionado);
				startActivity(i);
			}

		}
	};

	OnItemLongClickListener listenerLong = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View v,
				int position, long id) {
			posicaoSelecionada = position;
			builder = new AlertDialog.Builder(HomeActivity.this);
			builder.setMessage("Deseja excluir este cliente?")
			.setCancelable(false)
			.setPositiveButton("Sim",
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int id) {
					boolean deletou = UtilDB.deleteCliente(getApplicationContext(), listaClientes.get(posicaoSelecionada).getId());
					if (deletou) {
						carregaGridClientes();
					}
				}
			})
			.setNegativeButton("Não",
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int id) {
					dialog.cancel();
				}
			});

			builder.show();
			return false;
		}
	};

	

	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0,"Categorias")
		.setIcon(R.drawable.ic_action_config)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case 0:
			Intent i = new Intent("br.com.together.qtc.intentcategoria");
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
