package br.com.together.qtc.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import br.com.together.qtc.ClientePDVsActivity;
import br.com.together.qtc2.R;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc.vo.PDV;

public class PDVAdapter extends ArrayAdapter<PDV> {

	private ArrayList<PDV> items;
	private Context context;
	private Activity atividade;
	private View v;
	private Cliente cliente;

	public int getCount() {
		return items.size();
	}

	public PDVAdapter(Activity atividade, int textViewResourceId,
			ArrayList<PDV> items, Cliente cliente) {
		super(atividade.getApplicationContext(), textViewResourceId, items);
		this.items = items;
		this.context = atividade.getApplicationContext();
		this.atividade = atividade;
		this.cliente = cliente;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.pdv_item, null);
		}

		Resources res = atividade.getResources();
		v.setBackgroundDrawable((position & 1) == 1 ? res
				.getDrawable(R.drawable.linha_branca_selector) : res
				.getDrawable(R.drawable.linha_cinza_selector));

		final PDV t = items.get(position);

		if (t != null) {
			TextView nome = (TextView) v.findViewById(R.id.textNomePDV);
			int idPDV = t.getId();

			String nomePDV = t.getNome();
			nome.setText(nomePDV);

			TextView telefone = (TextView) v.findViewById(R.id.textTelefonePDV);

			String telefonePDV = t.getTelefone();
			telefone.setText(telefonePDV);

		}
		
		Button editar = (Button) v.findViewById(R.id.buttonEditarPDV);
		editar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent("br.com.together.qtc.intentcadastropdv");
				i.putExtra("cliente", cliente);
				i.putExtra("pdv",t);
				atividade.startActivity(i);
			}
		});
		
		Button visita = (Button) v.findViewById(R.id.buttonAgendarVisita);
		visita.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent("br.com.together.qtc.intentcadastrovisita");
				i.putExtra("cliente", cliente);
				i.putExtra("pdv",t);
				atividade.startActivity(i);
			}
		});

		return v;
	}

}
