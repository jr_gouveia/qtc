package br.com.together.qtc.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.together.qtc.vo.Categoria;
import br.com.together.qtc2.R;

public class CategoriaSelecionavelAdapter extends ArrayAdapter<Categoria> {

	private ArrayList<Categoria> items;
	private Context context;
	private Activity atividade;
	private View v;

	public int getCount() {
		return items.size();
	}

	public CategoriaSelecionavelAdapter(Activity atividade, int textViewResourceId,
			ArrayList<Categoria> items) {
		super(atividade.getApplicationContext(), textViewResourceId, items);
		this.items = items;
		this.context = atividade.getApplicationContext();
		this.atividade = atividade;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.custom_checkedtextview_holo_dark, null);
		}

		//v.setBackgroundColor((position & 1) == 1 ? Color.WHITE : Color.rgb(240, 240, 240));


		Categoria t = items.get(position);

		if (t != null) {
			//TextView nome = (TextView) v.findViewById(R.id.textNomeCategoriaList);
			TextView nome = (TextView) v.findViewById(android.R.id.text1);
			int idCategoria = t.getId();
			
			String nomeCategoria = t.getNome();
			nome.setText(nomeCategoria);
			
			
			
		}

		return v;
	}
	
	@Override
	public long getItemId(int position) {
		return getItem(position).getId();
	}

}
