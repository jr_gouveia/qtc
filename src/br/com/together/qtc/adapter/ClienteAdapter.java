package br.com.together.qtc.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.together.qtc2.R;
import br.com.together.qtc.vo.Cliente;

public class ClienteAdapter extends ArrayAdapter<Cliente> {

	private ArrayList<Cliente> items;
	private Context context;
	private Activity atividade;
	private View v;

	public int getCount() {
		return items.size();
	}

	public ClienteAdapter(Activity atividade, int textViewResourceId,
			ArrayList<Cliente> items) {
		super(atividade.getApplicationContext(), textViewResourceId, items);
		this.items = items;
		this.context = atividade.getApplicationContext();
		this.atividade = atividade;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.cliente_item, null);
		}

		//v.setBackgroundColor((position & 1) == 1 ? Color.WHITE : Color.rgb(240, 240, 240));


		Cliente t = items.get(position);

		if (t != null) {
			ImageView logo = (ImageView) v.findViewById(R.id.imageLogoClienteGrid);
			TextView nome = (TextView) v.findViewById(R.id.textNomeClienteGrid);
			int idCliente = t.getId();
			if (logo != null) {
				try{
					if(idCliente >= 0){
						String strLogo = t.getLogo();
						if(strLogo != null && strLogo.length() > 0){
							logo.setImageURI(Uri.parse(strLogo));
						}
					}else{
						logo.setImageResource(R.drawable.ic_launcher);
					}
					
				}catch (Exception e) {

				}
			}
			
			String nomeCliente = t.getNome();
//			if(t.getTitulo().length() > 52){
//				tituloVideo = tituloVideo.substring(0, 51);
//				int indice = tituloVideo.lastIndexOf(" ");
//				tituloVideo = tituloVideo.substring(0, indice);
//				tituloVideo += "...";
//			}
			nome.setText(nomeCliente);
			
			
			
		}

		return v;
	}
	

}
