package br.com.together.qtc.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.together.qtc2.R;
import br.com.together.qtc.vo.Visita;

public class VisitaAdapter extends ArrayAdapter<Visita> {

	private ArrayList<Visita> items;
	private Context context;
	private View v;
	private Activity atividade;

	public int getCount() {
		return items.size();
	}

	public VisitaAdapter(Activity atividade, int textViewResourceId,
			ArrayList<Visita> items) {
		super(atividade.getApplicationContext(), textViewResourceId, items);
		this.items = items;
		this.context = atividade.getApplicationContext();
		this.atividade = atividade;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.visita_item, null);
		}
		Resources res = atividade.getResources();
		v.setBackgroundDrawable((position & 1) == 1 ? res.getDrawable(R.drawable.linha_branca_selector): res.getDrawable(R.drawable.linha_cinza_selector));
		
		Visita t = items.get(position);

		if (t != null) {
			TextView nome = (TextView) v.findViewById(R.id.textNomePDV);
			int idPDV = t.getId();
			String nomePDV = t.getPdv().getNome();
			nome.setText(nomePDV);
			
			TextView data = (TextView) v.findViewById(R.id.textDataVisita);
			
			if(t.getData() != null){
				SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yy   HH:mm");
				data.setText(sf.format(t.getData().getTime()));
			}
			
			TextView nota = (TextView) v.findViewById(R.id.textNotaMediaVisita);
			nota.setVisibility(View.GONE);
			
		}

		return v;
	}
	

}
