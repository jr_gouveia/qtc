package br.com.together.qtc;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.TabHost;
import br.com.together.qtc.AvaliacaoFragmentActivity.AvaliacaoFragment;
import br.com.together.qtc.util.UtilDB;
import br.com.together.qtc.vo.Categoria;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc.vo.Visita;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class AvaliacaoActivity extends SherlockFragmentActivity {
	TabHost mTabHost;
	TabManager mTabManager;
	Cliente cliente;
	Visita visita;
	ArrayList<Categoria> listaCategorias;
	public String ultimaAba;
	boolean fechada = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.avaliacao);
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();

		ActionBar bar = getSupportActionBar();
		bar.setDisplayShowHomeEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);

		if (savedInstanceState != null) {
			cliente = (Cliente) savedInstanceState.getSerializable("cliente");
			visita = (Visita) savedInstanceState.getSerializable("visita");
		} else {
			if (getIntent().getExtras() != null) {
				Bundle extras = getIntent().getExtras();
				cliente = (Cliente) extras.getSerializable("cliente");
				visita = (Visita) extras.getSerializable("visita");
				if (extras.containsKey("anterior"))
					fechada = extras.getBoolean("anterior");
			}
		}

		if (cliente != null) {
			listaCategorias = UtilDB.getCategoriasDoCliente(
					getApplicationContext(), cliente);
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(
					"Desculpe, mas não foi possível carregar os dados!")
					.setCancelable(false)
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
									finish();
								}
							});
			AlertDialog alertDialog = builder.create();
			alertDialog.show();
		}

		mTabManager = new TabManager(this, mTabHost, R.id.realtabcontent);
		ultimaAba = "";
		mTabManager.idVisita = visita.getId();
		for (Categoria categoria : listaCategorias) {
			mTabManager.addTab(
					mTabHost.newTabSpec(String.valueOf(categoria.getId()))
							.setIndicator(categoria.getNome()),
					AvaliacaoFragmentActivity.AvaliacaoFragment.class, null);
			ultimaAba = String.valueOf(categoria.getId());
		}
		mTabManager.ultimaTab = ultimaAba;

		if (savedInstanceState != null) {
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
		}

		IntentFilter iF = new IntentFilter();
		iF.addAction("br.com.together.qtc.mudancadeaba");
		registerReceiver(mReceiver, iF);

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("tab", mTabHost.getCurrentTabTag());
		outState.putSerializable("cliente", cliente);
		outState.putSerializable("visita", visita);
	}

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d("QTC", "recebeu o broadcast");
			String tabAtual = mTabHost.getCurrentTabTag();
			int proxTab = -1;
			for (int i = 0; i < listaCategorias.size(); i++) {
				Categoria c = listaCategorias.get(i);
				String id = String.valueOf(c.getId());
				Log.d("QTC", "loop " + i);
				if (id.equalsIgnoreCase(tabAtual)) {
					Log.d("QTC", "loop achou");
					proxTab = i + 1;
					c = null;
					id = null;
					break;
				}
				c = null;
				id = null;
			}
			try {
				if (proxTab >= 0 && listaCategorias.size() > proxTab) {
					mTabManager.mTabHost.setCurrentTabByTag(String
							.valueOf(listaCategorias.get(proxTab).getId()));
				}
			} catch (Exception e) {
				Log.e("QTC", "", e);
			}
		}
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case android.R.id.home:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(
					"Tem certeza que deseja sair? \nTodas as alterações não salvas serão perdidas")
					.setCancelable(false)
					.setPositiveButton("Sim",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									finish();
								}
							})
					.setNegativeButton("Não",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			AlertDialog alertDialog = builder.create();
			alertDialog.show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * This is a helper class that implements a generic mechanism for
	 * associating fragments with the tabs in a tab host. It relies on a trick.
	 * Normally a tab host has a simple API for supplying a View or Intent that
	 * each tab will show. This is not sufficient for switching between
	 * fragments. So instead we make the content part of the tab host 0dp high
	 * (it is not shown) and the TabManager supplies its own dummy view to show
	 * as the tab content. It listens to changes in tabs, and takes care of
	 * switch to the correct fragment shown in a separate content area whenever
	 * the selected tab changes.
	 */
	public static class TabManager implements TabHost.OnTabChangeListener {
		private final FragmentActivity mActivity;
		private final TabHost mTabHost;
		private final int mContainerId;
		private final HashMap<String, TabInfo> mTabs = new HashMap<String, TabInfo>();
		TabInfo mLastTab;
		public String ultimaTab;
		public int idVisita;

		static final class TabInfo {
			private final String tag;
			private final Class<?> clss;
			private final Bundle args;
			private Fragment fragment;

			TabInfo(String _tag, Class<?> _class, Bundle _args) {
				tag = _tag;
				clss = _class;
				args = _args;
			}
		}

		static class DummyTabFactory implements TabHost.TabContentFactory {
			private final Context mContext;

			public DummyTabFactory(Context context) {
				mContext = context;
			}

			@Override
			public View createTabContent(String tag) {
				View v = new View(mContext);
				v.setMinimumWidth(0);
				v.setMinimumHeight(0);
				return v;
			}
		}

		public TabManager(FragmentActivity activity, TabHost tabHost,
				int containerId) {
			mActivity = activity;
			mTabHost = tabHost;
			mContainerId = containerId;
			mTabHost.setOnTabChangedListener(this);
		}

		public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
			tabSpec.setContent(new DummyTabFactory(mActivity));
			String tag = tabSpec.getTag();

			TabInfo info = new TabInfo(tag, clss, args);

			// Check to see if we already have a fragment for this tab, probably
			// from a previously saved state. If so, deactivate it, because our
			// initial state is that a tab isn't shown.
			info.fragment = mActivity.getSupportFragmentManager()
					.findFragmentByTag(tag);
			if (info.fragment != null && !info.fragment.isDetached()) {
				FragmentTransaction ft = mActivity.getSupportFragmentManager()
						.beginTransaction();
				ft.detach(info.fragment);
				ft.commit();
			}

			mTabs.put(tag, info);
			mTabHost.addTab(tabSpec);
		}

		@Override
		public void onTabChanged(String tabId) {
			TabInfo newTab = mTabs.get(tabId);
			if (mLastTab != newTab) {
				FragmentTransaction ft = mActivity.getSupportFragmentManager()
						.beginTransaction();
				if (mLastTab != null) {
					if (mLastTab.fragment != null) {
						((AvaliacaoFragment) mLastTab.fragment)
								.salvarAvaliacao();
						ft.detach(mLastTab.fragment);
					}
				}
				if (newTab != null) {
					if (newTab.fragment == null) {
						// newTab.fragment = Fragment.instantiate(mActivity,
						// newTab.clss.getName(), newTab.args);

						boolean ultima = false;
						if (tabId.equalsIgnoreCase(ultimaTab)) {
							ultima = true;
						}

						newTab.fragment = new AvaliacaoFragmentActivity.AvaliacaoFragment()
								.newInstance(Integer.parseInt(newTab.tag),
										ultima, idVisita);// FragmentStackSupport.CountingFragment.newInstance(mStackLevel);
						ft.add(mContainerId, newTab.fragment, newTab.tag);
					} else {
						ft.attach(newTab.fragment);
					}
				}

				mLastTab = newTab;
				ft.commit();
				mActivity.getSupportFragmentManager()
						.executePendingTransactions();
			}
		}
	}
}