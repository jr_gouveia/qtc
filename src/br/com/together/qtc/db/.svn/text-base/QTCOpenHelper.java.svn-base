package br.com.together.qtc.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import br.com.together.qtc.util.Util;
import br.com.together.qtc.vo.Categoria;

public class QTCOpenHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 7;
	private static final String DATABASE_NAME = "dbQTC";

	public static final String CLIENTE_TABLE_NAME = "QTC_CLIENTE";
	private static final String CLIENTE_TABLE_CREATE = "CREATE TABLE "
			+ CLIENTE_TABLE_NAME + " ("
			+
			// "_id INTEGER PRIMARY KEY, " +
			"_id_cliente INTEGER PRIMARY KEY, " + "nome TEXT, " + "logo TEXT, "
			+ "nome_responsavel TEXT, " + "telefone TEXT) ";

	public static final String CATEGORIAS_TABLE_NAME = "QTC_CATEGORIAS";
	private static final String CATEGORIAS_TABLE_CREATE = "CREATE TABLE "
			+ CATEGORIAS_TABLE_NAME + " (" +
			// "_id INTEGER PRIMARY KEY, " +
			"_id_categoria INTEGER PRIMARY KEY, " 
			+ "categoria TEXT, "
			+ "telefone TEXT)" ;

	private static final String CATEGORIAS_TABLE_ALTER = "ALTER TABLE "
			+ CATEGORIAS_TABLE_NAME + " ADD COLUMN" 
			+ "categoria TEXT " ;
	
	public static final String CLIENTES_CATEGORIAS_TABLE_NAME = "QTC_CLIENTES_CATEGORIAS";
	private static final String CLIENTES_CATEGORIAS_TABLE_CREATE = "CREATE TABLE "
			+ CLIENTES_CATEGORIAS_TABLE_NAME
			+ " ("
			+
			// "_id INTEGER PRIMARY KEY, " +
			"_id INTEGER PRIMARY KEY, "
			+ "id_cliente INTEGER, "
			+ "id_categoria INTEGER) ";

	public static final String PDVS_TABLE_NAME = "QTC_PDVS";
	private static final String PDVS_TABLE_CREATE = "CREATE TABLE "
			+ PDVS_TABLE_NAME + " (" + "_id_pdv INTEGER PRIMARY KEY, "
			+ "id_cliente INTEGER, " + "nome TEXT, " + "endereco TEXT, "
			+ "telefone TEXT, " + "nome_responsavel TEXT) ";

	private static final String PDVS_TABLE_DROP = "DROP TABLE "
			+ PDVS_TABLE_NAME + ";";

	public static final String PDVS_VISITAS_TABLE_NAME = "QTC_PDVS_VISITAS";
	private static final String PDVS_VISITAS_TABLE_CREATE = "CREATE TABLE "
			+ PDVS_VISITAS_TABLE_NAME + " (" + "_id_visita INTEGER PRIMARY KEY, "
			+ "id_pdv INTEGER, " + "id_cliente INTEGER, "
			+ "data TEXT) ";

	private static final String PDVS_VISITAS_TABLE_DROP = "DROP TABLE "
			+ PDVS_VISITAS_TABLE_NAME + ";";
	
	
	public static final String FOTOS_TABLE_NAME = "QTC_FOTOS";
	private static final String FOTOS_TABLE_CREATE = "CREATE TABLE "
			+ FOTOS_TABLE_NAME + " (" + "_id_foto INTEGER PRIMARY KEY, "
			+ "id_avaliacao INTEGER, "
			+ "media TEXT) ";
	
	public static final String AVALIACAO_TABLE_NAME = "QTC_AVALIACAO";
	private static final String AVALIACAO_TABLE_CREATE = "CREATE TABLE "
			+ AVALIACAO_TABLE_NAME + " (" + "_id_avaliacao INTEGER PRIMARY KEY, "
			+ "id_visita INTEGER, "
			+ "id_categoria INTEGER, "
			+ "comentarios TEXT, "
			+ "nota INTEGER) ";
	
	public QTCOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CLIENTE_TABLE_CREATE);
		Util.log("criou o banco de clientes: " + CLIENTE_TABLE_CREATE);
		db.execSQL(CATEGORIAS_TABLE_CREATE);
		Util.log("criou o banco de categorias: " + CATEGORIAS_TABLE_CREATE);
		db.execSQL(CLIENTES_CATEGORIAS_TABLE_CREATE);
		Util.log("criou o banco de clientes_categorias: "
				+ CLIENTES_CATEGORIAS_TABLE_CREATE);
		db.execSQL(PDVS_TABLE_CREATE);
		Util.log("criou o banco de pdvs: " + PDVS_TABLE_CREATE);
		db.execSQL(PDVS_VISITAS_TABLE_CREATE);
		Util.log("criou o banco de visitas: " + PDVS_VISITAS_TABLE_CREATE);
		db.execSQL(FOTOS_TABLE_CREATE);
		Util.log("criou o banco de fotos: " + FOTOS_TABLE_CREATE);
		db.execSQL(AVALIACAO_TABLE_CREATE);
		Util.log("criou o banco de avaliacao: " + AVALIACAO_TABLE_CREATE);
		populaCategorias(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (newVersion == 2 && oldVersion == 1) {
			db.execSQL(PDVS_TABLE_CREATE);
			Util.log("criou o banco de pdvs: " + PDVS_TABLE_CREATE);
		}
		if (newVersion == 3 && oldVersion == 2) {
			db.execSQL(PDVS_TABLE_DROP);
			Util.log("deletou o banco de pdvs: " + PDVS_TABLE_DROP);
		}
		if (newVersion == 4 && oldVersion == 3) {
			db.execSQL(PDVS_TABLE_CREATE);
			Util.log("criou o banco de pdvs: " + PDVS_TABLE_CREATE);
		}
		if (newVersion == 5 && oldVersion == 4) {
			db.execSQL(PDVS_VISITAS_TABLE_CREATE);
			Util.log("criou o banco de visitas: " + PDVS_VISITAS_TABLE_CREATE);
		}
		if (newVersion == 6 && oldVersion == 5) {
			db.execSQL(PDVS_VISITAS_TABLE_DROP);
			db.execSQL(PDVS_VISITAS_TABLE_CREATE);
			Util.log("criou o banco de visitas: " + PDVS_VISITAS_TABLE_CREATE);
		}
		if (newVersion == 7 && oldVersion == 6) {
			db.execSQL(CATEGORIAS_TABLE_ALTER);
			Util.log("alterou o banco de categorias: " + CATEGORIAS_TABLE_ALTER);
			db.execSQL(FOTOS_TABLE_CREATE);
			Util.log("criou o banco de fotos: " + FOTOS_TABLE_CREATE);
			db.execSQL(AVALIACAO_TABLE_CREATE);
			Util.log("criou o banco de avaliacao: " + AVALIACAO_TABLE_CREATE);
		}
	}

	private void populaCategorias(SQLiteDatabase db) {
		ArrayList<Categoria> listaCategorias = Util.inicializaCategorias();
		for (Categoria categoria : listaCategorias) {
			ContentValues values = new ContentValues();
			values.put("_id_categoria", categoria.getId());
			values.put("categoria", categoria.getNome());

			// tenta inserir, se o retorno for -1 é pq algum erro aconteceu
			long id = db.insert(CATEGORIAS_TABLE_NAME, null, values);
			Util.log("inseriu no banco a categoria " + categoria.getNome()
					+ " com o id " + id);
		}
	}

}