/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.together.qtc;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import br.com.together.qtc.util.Util;
import br.com.together.qtc.util.UtilDB;
import br.com.together.qtc.vo.Avaliacao;
import br.com.together.qtc.vo.Categoria;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class AvaliacaoFragmentActivity extends SherlockFragmentActivity {
	int mStackLevel = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_stack);

		// Watch for button clicks.
		Button button = (Button) findViewById(R.id.new_fragment);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				addFragmentToStack();
			}
		});

		if (savedInstanceState == null) {
			// Do first time initialization -- add initial fragment.
			Fragment newFragment = AvaliacaoFragment.newInstance(mStackLevel,
					false, 0);
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();
			ft.add(R.id.simple_fragment, newFragment).commit();
		} else {
			mStackLevel = savedInstanceState.getInt("level");
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("level", mStackLevel);
	}

	void addFragmentToStack() {
		mStackLevel++;

		// Instantiate a new fragment.
		Fragment newFragment = AvaliacaoFragment.newInstance(mStackLevel,
				false, 0);

		// Add the fragment to the activity, pushing this transaction
		// on to the back stack.
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.simple_fragment, newFragment);
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		ft.addToBackStack(null);
		ft.commit();
	}

	public static class AvaliacaoFragment extends SherlockFragment {
		int id;
		int idVisita;
		boolean ultimaAba;
		Avaliacao avaliacao;
		Categoria categoria;
		private static final int ACTION_TAKE_PHOTO_S = 2;
		private LinearLayout layoutFotos;
		private Bitmap mImageBitmap;
		private String mCurrentPhotoPath;
		private static final String JPEG_FILE_PREFIX = "IMG_";
		private static final String JPEG_FILE_SUFFIX = ".jpg";
		private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

		private RatingBar rating;
		private TextView labelRating;
		private EditText editComentarios;
		private ArrayList<String> listaFotosURI = new ArrayList<String>();

		/**
		 * Create a new instance of CountingFragment, providing "num" as an
		 * argument.
		 * 
		 * @param ultima
		 * @param idVisita
		 */
		static AvaliacaoFragment newInstance(int id, boolean ultima,
				int idVisita) {
			AvaliacaoFragment f = new AvaliacaoFragment();

			// Supply num input as an argument.
			Bundle args = new Bundle();
			args.putInt("id", id);
			args.putBoolean("ultima", ultima);
			args.putInt("idVisita", idVisita);
			f.setArguments(args);

			return f;
		}

		/**
		 * When creating, retrieve this instance's number from its arguments.
		 */
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			id = getArguments().getInt("id");
			idVisita = getArguments().getInt("idVisita");
			Log.d("QTC", "buscando avaliacao id categoria = " + id
					+ "  e id visita = " + idVisita);
			avaliacao = UtilDB
					.getAvaliacao(getSherlockActivity(), idVisita, id);
			if (getArguments().containsKey("ultima")) {
				ultimaAba = getArguments().getBoolean("ultima");
			} else {
				ultimaAba = false;
			}
			categoria = UtilDB.getCategoria(getSherlockActivity(), id);

			if (savedInstanceState != null) {
				if (savedInstanceState.containsKey("listaFotos")
						&& savedInstanceState.getSerializable("listaFotos") != null) {
					listaFotosURI = (ArrayList<String>) savedInstanceState
							.getSerializable("listaFotos");
					Log.d("QTC", "possui lista de fotos de tamanho "
							+ listaFotosURI.size());
				} else {
					Log.d("QTC", "não possui lista de fotos ou é nula");
				}
			} else {
				Log.d("QTC", "sem savedInstanceState");
			}
			Log.d("QTC", "FRAGMENT ON CREATE");

		}

		/**
		 * The Fragment's UI is just a simple text view showing its instance
		 * number.
		 */
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View v = inflater
					.inflate(R.layout.avaliacao_frag, container, false);
			if (categoria != null) {
				populaAvaliacao(v);
			}

			return v;
		}

		private void populaAvaliacao(View view) {
			TextView nome = (TextView) view
					.findViewById(R.id.TextViewNomeCategoria);
			if (nome != null) {
				nome.setText(categoria.getNome());
			}

			rating = (RatingBar) view.findViewById(R.id.ratingBarAvaliacao);
			editComentarios = (EditText) view
					.findViewById(R.id.editTextComentarios);

			if (avaliacao != null) {
				if (avaliacao.getRating() > 0) {
					rating.setRating(avaliacao.getRating());
				}
				editComentarios.setText(avaliacao.getComentarios());
				if (avaliacao.getId() > 0) {
					listaFotosURI = UtilDB.getFotosAvaliacao(
							getSherlockActivity().getApplicationContext(),
							avaliacao.getId());
				}
			}

			Button buttonLoadImage = (Button) view
					.findViewById(R.id.buttonAdicionarFoto);

			layoutFotos = (LinearLayout) view
					.findViewById(R.id.linearLayoutFotosAvaliacao);
			mImageBitmap = null;

			setBtnListenerOrDisable(buttonLoadImage, mTakePicSOnClickListener,
					MediaStore.ACTION_IMAGE_CAPTURE);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
				mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
			} else {
				mAlbumStorageDirFactory = new BaseAlbumDirFactory();
			}

			// MOMENTO DE RESTAURAR AS FOTOS JA TIRADAS
			if (listaFotosURI != null && (!listaFotosURI.isEmpty())) {
				for (String imagemPath : listaFotosURI) {
					try {
						setPic(imagemPath);
					} catch (Exception e) {
						Log.e("QTC", "erro ao tentar restaurar as fotos", e);
					}
				}

			}

			labelRating = (TextView) view
					.findViewById(R.id.textViewLabelRating);

			rating.setOnRatingBarChangeListener(listenerRating);

			Button buttonSalvarManter = (Button) view
					.findViewById(R.id.buttonSalvarContinuarAvaliacaoCategoria);
			Button buttonSalvar = (Button) view
					.findViewById(R.id.buttonSalvarAvaliacaoCategoria);
			Button buttonCancelar = (Button) view
					.findViewById(R.id.buttonCancelarAvaliacaoCategoria);

			if (ultimaAba) {
				buttonSalvar.setText("Salvar e Fechar");
			}

			buttonSalvarManter.setOnClickListener(listenerSalvarManter);
			buttonSalvar.setOnClickListener(listenerSalvar);
			buttonCancelar.setOnClickListener(listenerCancelar);

		}

		OnClickListener listenerSalvarManter = new OnClickListener() {

			@Override
			public void onClick(View v) {
				salvarAvaliacao();
			}
		};

		OnClickListener listenerSalvar = new OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean salvou = salvarAvaliacao();
				if (salvou) {
					if (ultimaAba) {
						getSherlockActivity().finish();
					} else {
						Log.d("QTC", "SALVOU e vai disparar o broadcast");
						Intent i = new Intent(
								"br.com.together.qtc.mudancadeaba");
						getSherlockActivity().sendBroadcast(i);
					}
				}

			}

		};

		public Boolean salvarAvaliacao() {

			try {
				if (rating != null && editComentarios != null) {
					float notaDada = rating.getRating();
					String comentarios = editComentarios.getText().toString();
					if (avaliacao == null) {
						avaliacao = new Avaliacao(idVisita, id);
					}
					avaliacao.setRating(notaDada);
					avaliacao.setComentarios(comentarios);
					avaliacao.setListaFotosURI(listaFotosURI);

					long id = -1;
					if (avaliacao.getId() > 0) {
						id = UtilDB.editAvaliacao(getSherlockActivity(),
								avaliacao);
						Toast.makeText(getSherlockActivity(), "Editando",
								Toast.LENGTH_SHORT).show();
					} else {
						id = UtilDB.saveAvaliacao(getSherlockActivity()
								.getApplicationContext(), avaliacao);
						Toast.makeText(getSherlockActivity(),
								"Salvando um novo", Toast.LENGTH_SHORT).show();
					}

					if (id > -1) {
						Toast.makeText(getSherlockActivity(),
								"Salvo com sucesso", Toast.LENGTH_SHORT).show();
						return true;
					} else {
						Toast.makeText(getSherlockActivity(),
								"Ocorreu um erro ao tentar salvar",
								Toast.LENGTH_SHORT).show();
						return false;
					}
				}else{
					return false;
				}
			} catch (Exception e) {
				Util.log("erro: ", e);
				return false;
			}
		}

		OnClickListener listenerCancelar = new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setMessage(
						"Tem certeza que deseja cancelar? Todas as alterações não salvas serão perdidas")
						.setCancelable(false)
						.setPositiveButton("Sim",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										getActivity().finish();
									}
								})
						.setNegativeButton("Não",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				AlertDialog alertDialog = builder.create();
				alertDialog.show();
			}
		};

		OnRatingBarChangeListener listenerRating = new OnRatingBarChangeListener() {

			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {

				if (rating == 0.0) {
					ratingBar.setRating((float) 1.0);
				} else if (rating == 1.0) {
					labelRating.setText("Muito Ruim");
				} else if (rating == 2.0) {
					labelRating.setText("Ruim");
				} else if (rating == 3.0) {
					labelRating.setText("Regular");
				} else if (rating == 4.0) {
					labelRating.setText("Bom");
				} else if (rating == 5.0) {
					labelRating.setText("Muito Bom");
				}
			}
		};

		private void dispatchTakePictureIntent(int actionCode) {

			Intent takePictureIntent = new Intent(
					MediaStore.ACTION_IMAGE_CAPTURE);

			File f = null;

			try {
				f = setUpPhotoFile();
				mCurrentPhotoPath = f.getAbsolutePath();
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(f));
			} catch (IOException e) {
				e.printStackTrace();
				f = null;
				mCurrentPhotoPath = null;
			}

			startActivityForResult(takePictureIntent, actionCode);
		}

		private File setUpPhotoFile() throws IOException {

			File f = createImageFile();
			mCurrentPhotoPath = f.getAbsolutePath();

			return f;
		}

		private String getAlbumName() {
			return "QTC";
		}

		private File getAlbumDir() {
			File storageDir = null;

			if (Environment.MEDIA_MOUNTED.equals(Environment
					.getExternalStorageState())) {

				storageDir = mAlbumStorageDirFactory
						.getAlbumStorageDir(getAlbumName());

				if (storageDir != null) {
					if (!storageDir.mkdirs()) {
						if (!storageDir.exists()) {
							Log.d("QTC", "failed to create directory");
							return null;
						}
					}
				}

			} else {
				Log.v(getString(R.string.app_name),
						"External storage is not mounted READ/WRITE.");
			}

			return storageDir;
		}

		private File createImageFile() throws IOException {
			// Create an image file name
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
					.format(new Date());
			String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
			File albumF = getAlbumDir();
			File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX,
					albumF);
			return imageF;
		}

		private void handleSmallCameraPhoto(Intent intent) {

			if (mCurrentPhotoPath != null) {
				setPic(mCurrentPhotoPath);
				galleryAddPic();
				listaFotosURI.add(mCurrentPhotoPath);
				Log.d("QTC", "PATH: " + mCurrentPhotoPath);
				mCurrentPhotoPath = null;
			}

		}

		Button.OnClickListener mTakePicSOnClickListener = new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchTakePictureIntent(ACTION_TAKE_PHOTO_S);
			}
		};

		@Override
		public void onActivityResult(int requestCode, int resultCode,
				Intent data) {
			switch (requestCode) {
			case ACTION_TAKE_PHOTO_S: {
				if (resultCode == RESULT_OK) {
					handleSmallCameraPhoto(data);
				}
				break;
			} // ACTION_TAKE_PHOTO_S

			} // switch
		}

		private void setPic(String photoPath) {

			/* Get the size of the ImageView */
			int targetW = 300;
			int targetH = 230;
			Log.d("QTC", "largura: " + targetW + "  altura: " + targetH);
			/* Get the size of the image */
			BitmapFactory.Options bmOptions = new BitmapFactory.Options();
			bmOptions.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(photoPath, bmOptions);
			int photoW = bmOptions.outWidth;
			int photoH = bmOptions.outHeight;

			/* Figure out which way needs to be reduced less */
			int scaleFactor = 1;
			if ((targetW > 0) || (targetH > 0)) {
				scaleFactor = Math.min(photoW / targetW, photoH / targetH);
			}
			Log.d("QTC", "scaleFactor: " + scaleFactor);

			/* Set bitmap options to scale the image decode target */
			bmOptions.inJustDecodeBounds = false;
			bmOptions.inSampleSize = scaleFactor;
			bmOptions.inPurgeable = true;

			/* Decode the JPEG file into a Bitmap */
			Bitmap bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);

			/* Associate the Bitmap to the ImageView */
			addImageView(bitmap);

		}

		private void addImageView(Bitmap foto) {
			ImageView imgView = new ImageView(getActivity()
					.getApplicationContext());

			LayoutParams lpImagem = new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.MATCH_PARENT);

			imgView.setLayoutParams(lpImagem);
			layoutFotos.addView(imgView);
			int pad = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, (float) 10.0, getResources()
							.getDisplayMetrics());
			imgView.setPadding(pad, pad, pad, pad);
			imgView.setScaleType(ScaleType.MATRIX);
			imgView.setImageBitmap(foto);
		}

		private void galleryAddPic() {
			Intent mediaScanIntent = new Intent(
					"android.intent.action.MEDIA_SCANNER_SCAN_FILE");
			File f = new File(mCurrentPhotoPath);
			Uri contentUri = Uri.fromFile(f);
			mediaScanIntent.setData(contentUri);
			getActivity().sendBroadcast(mediaScanIntent);
		}

		/**
		 * Indicates whether the specified action can be used as an intent. This
		 * method queries the package manager for installed packages that can
		 * respond to an intent with the specified action. If no suitable
		 * package is found, this method returns false.
		 * http://android-developers
		 * .blogspot.com/2009/01/can-i-use-this-intent.html
		 * 
		 * @param context
		 *            The application's environment.
		 * @param action
		 *            The Intent action to check for availability.
		 * 
		 * @return True if an Intent with the specified action can be sent and
		 *         responded to, false otherwise.
		 */
		public static boolean isIntentAvailable(Context context, String action) {
			final PackageManager packageManager = context.getPackageManager();
			final Intent intent = new Intent(action);
			List<ResolveInfo> list = packageManager.queryIntentActivities(
					intent, PackageManager.MATCH_DEFAULT_ONLY);

			return list.size() > 0;
		}

		private void setBtnListenerOrDisable(Button btn,
				Button.OnClickListener onClickListener, String intentName) {
			if (isIntentAvailable(getActivity(), intentName)) {
				btn.setOnClickListener(onClickListener);
			} else {
				btn.setText("Câmera não disponível");
				btn.setClickable(false);
			}
		}

		@Override
		public void onResume() {
			Log.d("QTC", "FRAGMENT ON RESUME");
			super.onResume();
		}

		@Override
		public void onSaveInstanceState(Bundle outState) {
			Log.d("QTC", "FRAGMENT ON SAVE");
			outState.putSerializable("listaFotos", listaFotosURI);
			super.onSaveInstanceState(outState);
		}

		@Override
		public void onInflate(Activity activity, AttributeSet attrs,
				Bundle savedInstanceState) {
			Log.d("QTC", "FRAGMENT ON INFLATE");
			super.onInflate(activity, attrs, savedInstanceState);
		}

	}

	@Override
	public void onResume() {
		Log.d("QTC", "ACTIVITY ON RESUME");
		super.onResume();
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		Log.d("QTC", "ACTIVITY ON RESTORE INSTANCE");
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	public Object onRetainCustomNonConfigurationInstance() {
		Log.d("QTC", "ACTIVITY ON RETAIN");
		return super.onRetainCustomNonConfigurationInstance();
	}

}
