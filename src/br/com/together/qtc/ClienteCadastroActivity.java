package br.com.together.qtc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import br.com.together.qtc.adapter.CategoriaSelecionavelAdapter;
import br.com.together.qtc.util.Util;
import br.com.together.qtc.util.UtilDB;
import br.com.together.qtc.vo.Categoria;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class ClienteCadastroActivity extends SherlockFragmentActivity {
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	String imagemUri;
	private ArrayList<Categoria> listaCategorias = new ArrayList<Categoria>();
	private Cliente cliente;
	EditText txtNome;
	EditText txtTelefone;
	EditText txtResponsavel;
	public static int MODO_EDICAO = 1;
	public static int MODO_INSERCAO = 2;
	private int modo = 2;
	ImageView logo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.cadastro_cliente);

		txtNome = (EditText) findViewById(R.id.EditTextNome);
		txtTelefone = (EditText) findViewById(R.id.editTextTelefone);
		txtResponsavel = (EditText) findViewById(R.id.EditTextResposavel);
		logo = (ImageView) findViewById(R.id.imageLogoDialog);
		carregaListaCategorias();
		if (getIntent().getExtras() != null) {
			Bundle extras = getIntent().getExtras();
			if(extras.containsKey("cliente")){
				cliente = (Cliente) extras.getSerializable("cliente");
				modo = MODO_EDICAO;
				populaDados(cliente);
			}else{
				cliente = null;
			}
		}


		Button buttonLoadImage = (Button) findViewById(R.id.buttonCarregaLogo);
		buttonLoadImage.setOnClickListener(listenerLogo);

		ActionBar bar = getSupportActionBar();

		bar.setTitle("Cadastro de cliente");
		bar.setDisplayShowHomeEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);


		Button botaoSalvar = (Button) findViewById(R.id.buttonSalvarCliente);
		botaoSalvar.setOnClickListener(listenerSalvar);
		Button botaoCancelar = (Button) findViewById(R.id.buttonCancelarCliente);
		botaoCancelar.setOnClickListener(listenerCancelar);
	}


	OnClickListener listenerLogo = new Button.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent(
					Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(intent, 0);
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			Uri targetUri = data.getData();
			imagemUri = targetUri.toString();

			logo.setImageURI(targetUri);

		}
	}

	OnClickListener listenerSalvar = new Button.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			long idCliente =  salvarCliente();
			long[] idsCategorias = pegaCategoriasSelecionadas();
			UtilDB.saveCategoriaDoCliente(getApplicationContext(), idsCategorias, idCliente);
			finish();
		}
	};

	OnClickListener listenerCancelar = new Button.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			finish();
		}
	};

	private void populaDados(Cliente c){
		txtNome.setText(cliente.getNome());
		txtTelefone.setText(cliente.getTelefone());
		txtResponsavel.setText(cliente.getResponsavel());
		imagemUri = cliente.getLogo();
		if(imagemUri != null && imagemUri.length() > 0){
			logo.setImageURI(Uri.parse(imagemUri));
		}

		ArrayList<Categoria> listaCategoriasCliente = UtilDB.getCategoriasDoCliente(getApplicationContext(), c);
		ListView listView = (ListView) findViewById(R.id.listViewCategorias);
		Util.log("CATEGORIAS DO CLIENTE: "+listaCategoriasCliente.size());
		//marca os selecionados
		for (Categoria catcliente : listaCategoriasCliente) {
			Util.log("id CATEGORIA DO CLIENTE: "+catcliente.getId());
			for (int j = 0; j < listaCategorias.size(); j++) {
				Categoria cat = listaCategorias.get(j);
				Util.log("id CATEGORIA: "+cat.getId());
				if (cat.getId() == catcliente.getId()){
					Util.log("IGUAL ");
					listView.setItemChecked(j, true);
				}
			}
		}
	}

	private void carregaListaCategorias(){
		listaCategorias = UtilDB.getCategorias(getApplicationContext());
		Collections.sort(listaCategorias, new Comparator<Categoria>() {
			public int compare(Categoria i1, Categoria i2) {
				return i1.getNome().toLowerCase().compareTo(i2.getNome().toLowerCase());
			}
		});
		ListView listView = (ListView) findViewById(R.id.listViewCategorias);
		listView.setVisibility(View.VISIBLE);
//		listView.setAdapter(new CategoriaSelecionavelAdapter(this,
//				R.layout.categoria_item_selecionavel, listaCategorias));
		listView.setAdapter(new CategoriaSelecionavelAdapter(this,
				R.layout.custom_checkedtextview_holo_dark, listaCategorias));



	}

	private long[] pegaCategoriasSelecionadas() {
		final StringBuffer sb = new StringBuffer("Selection: ");

		// Get an array that contains the IDs of the list items that are checked
		// --
		ListView listView = (ListView) findViewById(R.id.listViewCategorias);
		return listView.getCheckItemIds();

		//		if (checkedItemIds == null) {
		//			Toast.makeText(this, "No selection", Toast.LENGTH_LONG).show();
		//			return ;
		//		}
		//
		//		// For each ID in the status array
		//		// --
		//		boolean isFirstSelected = true;
		//		final int checkedItemsCount = checkedItemIds.length;
		//		for (int i = 0; i < checkedItemsCount; ++i) {
		//			if (!isFirstSelected) {
		//				sb.append(", ");
		//			}
		//			sb.append(checkedItemIds[i]);
		//			isFirstSelected = false;
		//		}
		//
		//		// Show a message with the country IDs that are selected
		//		// --
		//		Toast.makeText(this, sb.toString(), Toast.LENGTH_LONG).show();
	}

	private long salvarCliente() {
		//		txtNome = (EditText) findViewById(R.id.EditTextNome);
		//		txtTelefone = (EditText) findViewById(R.id.editTextTelefone);
		//		txtResponsavel = (EditText) findViewById(R.id.EditTextResposavel);
		String nome = "";
		String telefone = "";
		String responsavel = "";
		if (txtNome.getText().toString().trim().length() > 0) {
			nome = txtNome.getText().toString();
			telefone = txtTelefone.getText().toString();
			responsavel = txtResponsavel.getText().toString();
			if(modo == MODO_EDICAO){
				cliente.setNome(nome);
				cliente.setTelefone(telefone);
				cliente.setResponsavel(responsavel);
				cliente.setLogo(imagemUri);
				return UtilDB.editCliente(getApplicationContext(), cliente);
			}else{
				Cliente novoCliente = new Cliente(nome);
				novoCliente.setTelefone(telefone);
				novoCliente.setResponsavel(responsavel);
				novoCliente.setLogo(imagemUri);
				return UtilDB.saveCliente(getApplicationContext(), novoCliente);
			}
		}
		return -1;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case android.R.id.home:
			//			Intent intent = new Intent(this, HomeActivity.class);
			//			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//			startActivity(intent);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
