package br.com.together.qtc;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import br.com.together.qtc.adapter.CategoriaAdapter;
import br.com.together.qtc.util.UtilDB;
import br.com.together.qtc.vo.Categoria;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class CategoriaActivity extends SherlockFragmentActivity {
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */

	private ArrayList<Categoria> listaCategorias = new ArrayList<Categoria>();
	private View layout;
	private AlertDialog.Builder builder;
	private Categoria categoriaSelecionada;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.categoria_activity);

		ActionBar bar = getSupportActionBar();

		bar.setTitle("Categorias");
		bar.setDisplayShowHomeEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);
		carregaListaCategorias();

	}

	private void carregaListaCategorias() {
		listaCategorias = UtilDB.getCategorias(getApplicationContext());
		ListView listView = (ListView) findViewById(R.id.listViewCategorias);
		listView.setVisibility(View.VISIBLE);
		listView.setAdapter(new CategoriaAdapter(this, R.layout.categoria_item,
				listaCategorias));
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				mostraDialog(listaCategorias.get(position));
			}

		});

	}

	private void mostraDialog(Categoria cat) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layout = inflater.inflate(R.layout.dialog_cadastro_categoria,
				(ViewGroup) findViewById(R.id.rootCadastroCategoria));
		final EditText txtNome = (EditText) layout
				.findViewById(R.id.EditTextNomeCategoria);
		final EditText txtInstrucao = (EditText) layout
				.findViewById(R.id.EditTextInstrucaoCategoria);
		if (cat != null) {

			txtNome.setText(cat.getNome());
			txtInstrucao.setText(cat.getInstrucao());
			categoriaSelecionada = cat;
		}
		builder = new AlertDialog.Builder(this).setView(layout);
		// builder.setMessage("Cadastro de um novo cliente")
		builder.setCancelable(false)
				.setPositiveButton("Salvar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								boolean inseriu = false;
								if (categoriaSelecionada != null) {
									if (txtNome.getText().toString().trim()
											.length() > 0) {
										categoriaSelecionada.setNome(txtNome
												.getText().toString());
										categoriaSelecionada
												.setInstrucao(txtInstrucao
														.getText().toString());
										inseriu = salvarCategoria(categoriaSelecionada);
									}
								} else {
									inseriu = salvarCategoria(null);
								}
								if (inseriu) {
									carregaListaCategorias();
								}
							}
						})
				.setNegativeButton("Cancelar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		builder.show();

	}

	private boolean salvarCategoria(Categoria categoria) {
		if (categoria == null) {
			EditText txtNome = (EditText) layout
					.findViewById(R.id.EditTextNomeCategoria);
			if (txtNome.getText().toString().trim().length() > 0) {
				String nome = txtNome.getText().toString();
				Categoria novaCategoria = new Categoria(nome);
				EditText txtInstrucao = (EditText) layout
						.findViewById(R.id.EditTextInstrucaoCategoria);
				if (txtInstrucao.getText().toString().trim().length() > 0) {
					String instrucao = txtInstrucao.getText().toString();
					novaCategoria.setInstrucao(instrucao);
					return UtilDB.saveCategoria(getApplicationContext(),
							novaCategoria);
				}
				return UtilDB.saveCategoria(getApplicationContext(),
						novaCategoria);
			}
		} else {
			return UtilDB.editCategoria(getApplicationContext(), categoria);
		}

		return false;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "adicionar")
				.setIcon(R.drawable.ic_action_adicionar)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_ALWAYS
								| MenuItem.SHOW_AS_ACTION_WITH_TEXT);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case 0:
			mostraDialog(null);
			return true;
		case android.R.id.home:
			Intent intent = new Intent(this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
