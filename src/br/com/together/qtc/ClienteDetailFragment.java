package br.com.together.qtc;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.SherlockFragment;

public class ClienteDetailFragment extends SherlockFragment {
	Cliente cliente;
	private View mContentView;
	Activity atividade;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey("cliente")) {
				cliente = (Cliente) savedInstanceState
						.getSerializable("cliente");
				// mCurPosition = savedInstanceState.getInt("listPosition");
			}
		}
		atividade = getActivity();
		carregaFragmento(cliente);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.cliente_detail_frag, null);

		mContentView.setDrawingCacheEnabled(false);
		return mContentView;
	}

	public void carregaFragmento(Cliente cliente) {
		
		if (cliente != null) {
			this.cliente = cliente;
			TextView nome = (TextView) mContentView
					.findViewById(R.id.textViewNomeClienteDetail);
			TextView telefone = (TextView) mContentView
					.findViewById(R.id.textViewTelefoneClienteDetail);
			TextView responsavel = (TextView) mContentView
					.findViewById(R.id.textViewResponsavelClienteDetail);
			ImageView logo = (ImageView) mContentView
					.findViewById(R.id.imageViewLogoClienteDetail);
			
			nome.setText(cliente.getNome());
			telefone.setText(cliente.getTelefone());
			responsavel.setText(cliente.getResponsavel());
			String uriLogo = cliente.getLogo();
			if(uriLogo!= null && uriLogo.length()>0){
				logo.setImageURI(Uri.parse(uriLogo));
			}
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("cliente", cliente);
	}

}
