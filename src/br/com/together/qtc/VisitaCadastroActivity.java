package br.com.together.qtc;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import br.com.together.qtc.util.UtilDB;
import br.com.together.qtc.vo.Cliente;
import br.com.together.qtc.vo.PDV;
import br.com.together.qtc.vo.Visita;
import br.com.together.qtc2.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class VisitaCadastroActivity extends SherlockFragmentActivity {
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	String imagemUri;
	private Cliente cliente;
	private PDV pdv;
	private Visita visita;
	ArrayList<PDV> listPDVs;
	Spinner spinnerPDV;
	TimePicker horarioPicker;
	DatePicker dataPicker;
	boolean selecionaPDV = false;
	public static int MODO_EDICAO = 1;
	public static int MODO_INSERCAO = 2;
	private int modo = 2;
	static final int DATE_DIALOG_ID = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.cadastro_visita);

		if (getIntent().getExtras() != null) {
			Bundle extras = getIntent().getExtras();
			if (extras.containsKey("cliente")) {
				cliente = (Cliente) extras.getSerializable("cliente");
				populaDadosCliente(cliente);
			} else {
				cliente = null;
			}

			if (extras.containsKey("pdv")) {
				pdv = (PDV) extras.getSerializable("pdv");
				selecionaPDV = true;
			} else {
				pdv = null;
			}

			if (extras.containsKey("visita")) {
				visita = (Visita) extras.getSerializable("visita");
				modo = MODO_EDICAO;
			} else {
				visita = null;
			}
		}
		
		listPDVs = UtilDB.getPDVs(this.getApplicationContext(), cliente);
		ArrayList<String> listNomesPDVs = new ArrayList<String>();
		for (PDV p : listPDVs) {
			listNomesPDVs.add(p.getNome());
		}
		spinnerPDV = (Spinner) findViewById(R.id.spinnerPDV);
		spinnerPDV.setAdapter(new ArrayAdapter(this,
				android.R.layout.simple_spinner_dropdown_item, listNomesPDVs));

		if(selecionaPDV){
			selecionaPDV(pdv);
		}
		ActionBar bar = getSupportActionBar();

		bar.setTitle("Agendamento de Visita");
		bar.setDisplayShowHomeEnabled(true);
		bar.setDisplayHomeAsUpEnabled(true);

		dataPicker = (DatePicker) findViewById(R.id.datePickerVisita);

		horarioPicker = (TimePicker) findViewById(R.id.timePickerVisita);
		horarioPicker.setIs24HourView(true);

		Button botaoSalvar = (Button) findViewById(R.id.buttonSalvarVisita);
		botaoSalvar.setOnClickListener(listenerSalvar);
		Button botaoCancelar = (Button) findViewById(R.id.buttonCancelarVisita);
		botaoCancelar.setOnClickListener(listenerCancelar);
	}

	OnClickListener listenerSalvar = new Button.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			long idVisita = salvarVisita();
			if (idVisita >= 0) {
				finish();
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						VisitaCadastroActivity.this);
				builder.setMessage(
						"Não foi possível salvar esta visita, verifique se o PDV, a data e hora da visita foram selecionados corretamente.")
						.setCancelable(false)
						.setNegativeButton("fechar",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				AlertDialog alertDialog = builder.create();
				alertDialog.show();
			}
		}
	};

	OnClickListener listenerCancelar = new Button.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			finish();
		}
	};

	private void populaDados(PDV pdv) {
		
	}

	private void populaDadosCliente(Cliente c) {
		ClienteDetailFragment clienteFrag = (ClienteDetailFragment) getSupportFragmentManager()
				.findFragmentById(R.id.frag_clienteDetails);
		clienteFrag.carregaFragmento(cliente);

	}

	private void selecionaPDV(PDV pdv) {
		for (int i = 0; i < listPDVs.size(); i++) {
			PDV p = listPDVs.get(i);
			if (p.getId() == pdv.getId()) {
				spinnerPDV.setSelection(i);
			}
		}

	}

	private long salvarVisita() {
		int posicaoSelecionada = spinnerPDV.getSelectedItemPosition();
		PDV pdvSelecionado = listPDVs.get(posicaoSelecionada);

		int dia = dataPicker.getDayOfMonth();
		int mes = dataPicker.getMonth();
		int ano = dataPicker.getYear();
		int hora = horarioPicker.getCurrentHour();
		int minuto = horarioPicker.getCurrentMinute();

		GregorianCalendar data = new GregorianCalendar(ano, mes, dia, hora,
				minuto);

		if (modo == MODO_EDICAO) {
			visita.setData(data);
			return UtilDB.editVisita(getApplicationContext(), visita);
		} else {
			Visita novaVisita = new Visita();
			novaVisita.setPdv(pdvSelecionado);
			novaVisita.setData(data);
			return UtilDB.saveVisita(getApplicationContext(), novaVisita,
					cliente);
		}

		// return -1;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case android.R.id.home:
			// Intent intent = new Intent(this, HomeActivity.class);
			// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// startActivity(intent);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
